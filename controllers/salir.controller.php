<?php

class SalirController
{
    
    public $utilidades;    

    public function __construct(){        
        $this->utilidades = new Utilidades();        
    }

    public function Index(){        
        $this->utilidades->session_end();
        header('Location: /');
    }
            
}