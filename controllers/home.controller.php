<?php

require_once PATH.'/models/home.php';

class HomeController
{

    private $model;
    public $utilidades;
    public $groupMsg;

    /**
     * Constructor de contralador Home
     */
    public function __construct(){
        $this->model = new Home();
        $this->utilidades = new Utilidades();        
    }

    /**
     * Metodo por defecto
     */
    public function Index(){
        require_once HEADER;
        require_once PATH.'/views/home/home.php';
        require_once FOOTER;
    }    
    
}