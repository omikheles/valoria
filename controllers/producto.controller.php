<?php

require_once PATH.'/models/producto.php';

class ProductoController
{

    private $model;
    public $utilidades;

    /**
     * Constructor producto
     */
    public function __construct(){
        
        $this->model = new Producto();
        $this->utilidades = new Utilidades();

        $this->model->id_producto = !empty($_GET['id']) ? $this->utilidades->sanitize($_GET['id']) : 0;

        // Asignamos los valores de campos segun el submit de registro, perfil o login
        if(isset($_POST['valorar-producto']) || isset($_POST['valorar-modificar-producto'])){
            $this->model->titulo_opinion = !empty($_POST['titulo_opinion']) ? $_POST['titulo_opinion'] : null;
            $this->model->cuerpo_opinion = !empty($_POST['cuerpo_opinion']) ? $_POST['cuerpo_opinion'] : null;        
            $this->model->stars_opinion = !empty($_POST['stars_opinion']) ? $_POST['stars_opinion'] : null;
        }

    }

    /**
     * Mostramos el producto completo
     */
    public function Index(){                    

        if(!$producto = $this->model->obtenerProducto($this->model->id_producto)){
            header('Location: /');
        }

        require_once HEADER;
        require_once PATH.'/views/producto/producto.completo.php';
        require_once FOOTER;
    }            

    /**
     * Valorar producto
     */
    public function valorar()
    {
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }
            
        $opinion = $this->model->obtenerOpinion($this->model->id_usuario,$this->model->id_producto);        
        
        if(isset($_POST['valorar-producto']) && $this->model->valorar_producto($this->model)){            
            header('Location: /producto?id='.$this->model->id_producto);
        }

        if(isset($_POST['valorar-modificar-producto']) && $this->model->modificarOpinion($this->model)){            
            header('Location: /producto?id='.$this->model->id_producto);
        }
        
        require_once HEADER;
        require_once $opinion ? PATH.'/views/producto/valorar.modificar.php' : PATH.'/views/producto/valorar.php';                
        require_once FOOTER;
    }
    
}