<?php

require_once PATH.'/models/admin.php';

class AdminController
{

    private $model;
    public $utilidades;

    /**
     * Constructor de Admin
     */
    public function __construct(){
        
        $this->model = new Admin();
        $this->utilidades = new Utilidades();

        if(isset($_POST['crear-producto']) || isset($_POST['editar-producto'])){
            $this->model->titulo_producto = !empty($_POST['titulo_producto']) ? $_POST['titulo_producto'] : null;
            $this->model->cuerpo_producto = !empty($_POST['cuerpo_producto']) ? $_POST['cuerpo_producto'] : null;        
            $this->model->categoria_producto = !empty($_POST['categoria_producto']) ? $_POST['categoria_producto'] : null;            
            $this->model->ubicacion_producto = !empty($_POST['ubicacion_producto']) ? $_POST['ubicacion_producto'] : null;
            $this->model->web_producto = !empty($_POST['web_producto']) ? $_POST['web_producto'] : null;
            $this->model->email_producto = !empty($_POST['email_producto']) ? $_POST['email_producto'] : null;
            $this->model->phone_producto = !empty($_POST['phone_producto']) ? $_POST['phone_producto'] : null;
        }

    }

    /**
     * Panel de usuario normal o administrador
     */
    public function Index(){

        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }
                     
        require_once HEADER;
        require_once PATH.'/views/admin/admin.php';
        require_once FOOTER;
    }

    /**
     * Crear producto nuevo
     */
    public function crear_producto()
    {
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }       

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }
        
        if(isset($_POST['crear-producto']) && $this->model->crear_producto($this->model)){
            header('Location: /admin');
        }

        require_once HEADER;
        require_once PATH.'/views/admin/crear.producto.php';
        require_once FOOTER;
    }

    /**
     * Editar producto
     */
    public function editar_producto()
    {
                
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(empty($_GET['id'])){
            header('Location: /admin');    
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }

        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;
            
        $this->model->id_producto = $id;            

        if(!$producto = $this->model->obtenerProducto($id)){
            header('Location: /admin');
        }
        
        if(isset($_POST['editar-producto']) && $this->model->actualizar_producto($this->model)){
            header('Location: /admin/editar-producto?id='.$this->model->id_producto.'#actualizado');
        }

        require_once HEADER;
        require_once PATH.'/views/admin/editar.producto.php';
        require_once FOOTER;
    }

    /**
     * Eliminar producto
     */
    public function eliminar_producto()
    {
        
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }

        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;      
                        
        if($this->model->eliminar_producto($id)){
            header('Location: /admin#eliminado');           
        } else {            
            require_once HEADER;
            require_once PATH.'/views/admin/admin.php';
            require_once FOOTER;
        }

    }

    /**
     * Administrar los usuarios
     */
    public function usuarios(){
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }
                     
        require_once HEADER;
        require_once PATH.'/views/admin/admin.usuarios.php';
        require_once FOOTER;
    }

    /**
     * Eliminar usuario
     */
    public function eliminar_usuario()
    {
        
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(empty($_GET['id'])){
            header('Location: /');
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }

        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;      
                        
        if($this->model->eliminar_usuario($id)){
            header('Location: /admin/usuarios#eliminado');
        }        

    }

    /**
     * Administrar valoraciones
     */
    public function valoraciones(){
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }
                     
        require_once HEADER;
        require_once PATH.'/views/admin/admin.valoraciones.php';
        require_once FOOTER;
    }

    /**
     * Eliminar opinion
     */
    public function eliminar_opinion()
    {
        
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        if(empty($_GET['id'])){
            header('Location: /');
        }

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            header('Location: /');
        }

        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;      
                        
        if($this->model->eliminarOpinionPorId($id)){
            header('Location: /admin/valoraciones#eliminado');
        }        

    }
    
}