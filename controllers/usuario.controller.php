<?php

require_once PATH.'/models/usuario.php';

class UsuarioController
{

    private $model;
    public $utilidades;

    /**
     * Constructor usuario
     */
    public function __construct(){
        
        $this->model = new Usuario();
        $this->utilidades = new Utilidades();

        // Asignamos los valores de campos segun el submit de registro, perfil o login
        if(isset($_POST['registrar']) || isset($_POST['editar'])){
            $this->model->id = !empty($_POST['id']) ? $_POST['id'] : 0;
            $this->model->nombre = !empty($_POST['nombre']) ? $_POST['nombre'] : null;
            $this->model->apellidos = !empty($_POST['apellidos']) ? $_POST['apellidos'] : null;
            $this->model->email = !empty($_POST['email']) ? $_POST['email'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
            $this->model->contrasena2 = !empty($_POST['contrasena2']) ? md5($_POST['contrasena2']) : null;
        } elseif(isset($_POST['login'])){
            $this->model->email = !empty($_POST['email']) ? $_POST['email'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
        }

    }

    /**
     * El formulario de registro
     */
    public function Index(){

        if($this->utilidades->checkLogin()){
            header('Location: /');
        }
        

        if(isset($_POST['registrar']) && $this->model->registrar($this->model)){
            $_SESSION['login'] = $this->model->email;
            $_SESSION['id'] = $this->model->id;
            header('Location: /usuario#registrado');            
        }

        require_once HEADER;
        require_once PATH.'/views/usuario/registro.php';
        require_once FOOTER;
    }
        
    /**
     * El formulario de editar perfil. Antes de mostrar comprobamos si el usuario esta autorizado y recuperamos el objeto desde la base.
     */
    public function editar(){
        
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }
            
        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : $_SESSION['id'];

        if(Utilidades::tipoUsuario($_SESSION['id']) !== 'administrador'){
            $user = $this->model->obtener($_SESSION['id']);
        } else {
            // Necesitamos la siguiente lógica para dar permisos a administrador editar cualquier perfil de usuario.
            if(!empty($id) && $this->model->obtener($id)){
                $user = $this->model->obtener($id);
            } else {
                $user = $this->model->obtener($_SESSION['id']);
            }                
        }

        if(isset($_POST['editar']) && $this->model->actualizar($this->model)){            
            header('Location: /usuario/editar?id='.$id.'#actualizado');            
        }
                
        require_once HEADER;
        require_once PATH.'/views/usuario/editar.php';
        require_once FOOTER;
    }

    public function login(){

        if($this->utilidades->checkLogin()){
            header('Location: /');
        }
        
        if(isset($_POST['login'])){
                        
            $email = $this->model->email;
            $password = $this->model->contrasena;
            
            if($rs = $this->model->login($email,$password)){
                                
                $_SESSION['login'] = $email;
                $_SESSION['id'] = $rs['Id'];

                if($rs['id_tipo_usuario'] == 2){
                    header('Location: /');
                } else {
                    header('Location: /admin');
                    // TODO: Admin panel
                }                
                                
            } else {
                $this->model->errors = 'Usuario no existe. Asegura que el e-mail y contraseña son correctos.';
            }
        }

        require_once HEADER;
        require_once PATH.'/views/usuario/login.php';
        require_once FOOTER;
    }
    
}