<section class="p-5 producto">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-10">                                                          

                <?php   
                    // var_dump($producto); 
                    // var_dump($this->model->obtenerOpinion($this->model->id_usuario,$this->model->id_producto));                
                ?>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-5 d-flex align-items-center">                                                        
                            <?php if($this->model->obtenerOpinion($this->model->id_usuario,$this->model->id_producto)): ?>                            
                            <span class="producto__tag mr-2">Producto valorado por usted</span>
                            <img src="/assets/img/star.svg" width="20" height="20" class="mr-2" alt="" />
                            <a href="/producto/valorar?id=<?php echo $producto->Id; ?>" class="strong-link">Modificar opinión</a>
                            <?php else: ?>
                            <img src="/assets/img/star.svg" width="20" height="20" class="mr-2" alt="" />
                            <a href="/producto/valorar?id=<?php echo $producto->Id; ?>" class="strong-link">Valorar producto</a>
                            <?php endif; ?>                            
                        </div>
                    </div>
                </div>

                <div class="row d-flex align-items-center mb-5">
                    <?php if(!empty($producto->Imagen)): ?>
                    <div class="col-md-6">                        
                        <div class="producto__image mb-5 mb-md-0 d-none d-md-block" style="background-image:url(<?php echo $producto->Imagen; ?>);"></div>
                        <img class="producto__image-mobile d-md-none" src="<?php echo $producto->Imagen; ?>" />
                    </div>
                    <?php endif; ?>
                    <div class="<?php echo empty($producto->Imagen) ? 'col-md-12' : 'col-md-6'; ?>">                
                        
                        <div class="product__details">                                                        
                            
                            <?php if(!empty($producto->Web)): ?>
                            <div class="product__details-unit pt-0 p-3"><img src="/assets/img/link.svg" width="30" class="mr-3" alt="" /><a href="<?php echo $producto->Web; ?>" target="_blank"><?php echo $producto->Web; ?></a></div>
                            <?php endif; ?>

                            <?php if(!empty($producto->Email)): ?>
                            <div class="product__details-unit p-3"><img src="/assets/img/mail.svg" width="30" class="mr-3" alt="" /><?php echo $producto->Email; ?></div>
                            <?php endif; ?>

                            <?php if(!empty($producto->Phone)): ?>
                            <div class="product__details-unit p-3"><img src="/assets/img/phone.svg" width="30" class="mr-3" alt="" /><?php echo $producto->Phone; ?></div>
                            <?php endif; ?>                                                        
                            
                        </div>             

                        <!-- <div class="text-right"><a href="/producto/valorar?id=<?php echo $producto->Id; ?>" class="strong-link">Valorar producto</a></div> -->

                    </div>
                </div>                
                
                <h2 class="mb-5 decorate decorate--left">Descripción de <strong>producto</strong> o <strong>servicio</strong></h2>

                <?php echo $producto->Descripcion; ?>

                <hr class="mt-5 mb-5"/>

                <div class="d-flex align-items-center">                    
                    <h2 id="opiniones" class="decorate decorate--left mr-auto"><?php echo Utilidades::countOpinionesProducto($producto->Id); ?> Opiniones de <strong>clientes</strong></h2>
                    <a href="/producto/valorar?id=<?php echo $producto->Id; ?>" class="button">Valorar producto</a>
                </div>

                <?php if($this->model->listar_opiniones($producto->Id)): ?>

                <?php foreach($this->model->listar_opiniones($producto->Id) as $opinion): ?>

                <?php //var_dump($opinion); ?>

                <div class="opinion pt-5 pb-5">
                    <div class="opinion__nombre mb-3"><img src="/assets/img/avatar.svg" width="40" class="mr-3" alt="" /><?php echo Utilidades::nombreUsuario($opinion->Id_usuario); ?></div>                    
                    <div class="d-flex align-items-center mb-3">
                        <div class="rating__stars mr-2"><?php echo Utilidades::estrellasProducto($opinion->Valoracion,true); ?></div>                        
                    </div>
                    <div class="opinion__title"><?php echo $opinion->Titulo; ?></div>
                    <div class="opinion__date mb-3">Publicado: <?php echo $opinion->Fecha_valoracion; ?></div>
                    <div class="opinion__text"><?php echo $opinion->Opinion; ?></div>
                </div>                

                <?php endforeach; ?>

                <?php else: ?>        
                <div class="text-center">
                    <p class="mb-5">Aun no hay valoraciones, ¡Sé el primero!</p>
                    <a href="/producto/valorar?id=<?php echo $producto->Id; ?>" class="button scrollto">Valorar producto</a> 
                </div>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section>