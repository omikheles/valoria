<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-10">
                <h2>Valorar producto o servicio</h2>
                
                <div class="errors text-center mt-5 mb-5"><?php echo $this->model->errors; ?></div>
                    <form class="admin-form-producto" action="/producto/valorar?id=<?php echo $this->model->id_producto; ?>" method="POST">                        
                        <div class="form-row">                            
                            <div class="form-group col-md-12">                                    
                                <div class="rating__stars mb-3">  
                                    <p>Valora de 1 a 5</p>                           
                                    <span class="rating__star"></span><span class="rating__star"></span><span class="rating__star"></span><span class="rating__star"></span><span class="rating__star"></span>
                                    
                                    <input type="radio" name="stars_opinion" value="1"<?php echo $this->model->stars_opinion == 1 ? ' checked' : ''; ?>>
                                    <input type="radio" name="stars_opinion" value="2"<?php echo $this->model->stars_opinion == 2 ? ' checked' : ''; ?>>
                                    <input type="radio" name="stars_opinion" value="3"<?php echo $this->model->stars_opinion == 3 ? ' checked' : ''; ?>>
                                    <input type="radio" name="stars_opinion" value="4"<?php echo $this->model->stars_opinion == 4 ? ' checked' : ''; ?>>
                                    <input type="radio" name="stars_opinion" value="5"<?php echo $this->model->stars_opinion == 5 ? ' checked' : ''; ?>>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-row">                            
                            <div class="form-group col-md-12">                                    
                                <input type="text" class="form-control" id="titulo" name="titulo_opinion" placeholder="Título" value="<?php echo $this->model->titulo_opinion; ?>">                                
                            </div>
                        </div>                                                                        
                        <div class="form-row">
                            <div class="form-group col">
                                <textarea class="form-control" id="cuerpo_opinion" name="cuerpo_opinion" rows="3" placeholder="Escribir vuestra opinión"><?php echo $this->model->cuerpo_opinion; ?></textarea>
                            </div>
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">                                
                                <button type="submit" name="valorar-producto" class="button">Publicar</button>
                            </div>
                        </div>                        
                    </form>

            </div>
        </div>
    </div>
</section>