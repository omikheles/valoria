<!doctype html>
<html lang="es">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Oswald:500,700" rel="stylesheet">    
    <link rel="stylesheet" href="/assets/css/main.css?v=1557879681">

    <title>Valoria - tu opinión importa.</title>
  </head>
  <body>

    <div class="hero<?php echo ($this->model->page == 'home') ? '' : ' hero--inner'; ?>">

        <header class="cabecera pt-2 pb-2">
            <div class="container">
                <nav class="navbar navbar-light navbar-expand-lg">
                    
                    <a class="navbar-brand mr-5" href="/"><img src="/assets/img/logo.svg" width="200px" alt="" /></a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active"><a class="nav-link scrollto" href="/">Inicio</a></li>
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorías</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                                    <a class="dropdown-item" href="/?id_cat=<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></a>   
                                    <?php endforeach; ?>                                    
                                </div>
                            </li>                            
                            <!-- <li class="nav-item"><a class="nav-link scrollto" href="#servicios">Sobre nosotros</a></li> -->
                            <li class="nav-item"><a class="nav-link scrollto" href="/#contactar">Contactar</a></li>
                        </ul>
                        <div class="extras">
                            
                            <?php if($this->model->utilidades->checkLogin()): ?>
                            <a href="/usuario/editar" class="mr-3">Editar perfil</a><a href="/salir">Cerrar sesión</a>
                            <?php else: ?>
                            <a href="/usuario/login">Iniciar sesión</a>
                            <?php endif; ?>
                        </div>
                    </div>

                </nav>
            </div>
        </header>

        <div class="container">
            <div class="row">
                <div class="col text-center">                    
                    <?php if($this->model->page !== 'home'): ?>
                    <div class="mb-3"><img src="/assets/img/heart.svg" width="40" alt="Valoria" /></div>
                    <?php endif; ?>          
                    
                    <?php if($this->model->page == 'producto' && $this->model->action == '' ): ?>
                        <div class="hero__micro-title mb-3"><?php echo Utilidades::obtenerNombreCategoria($producto->Id_categoria); ?></div>
                        <div class="hero__title mb-3"><?php echo $producto->Titulo; ?></div>
                        <div class="rating">
                            <div class="rating__stars mb-3"><?php echo Utilidades::estrellasProducto(Utilidades::obtenerPesoValoraciones($producto->Id)); ?></div>
                            <div class="rating__text"><?php echo Utilidades::obtenerPesoValoraciones($producto->Id); ?> basado en <a href="#opiniones"><?php echo Utilidades::countOpinionesProducto($producto->Id); ?> opiniones</a></div>                            
                        </div>                 
                    <?php else: ?>
                        <div class="hero__micro-title mb-3"><?php echo $this->model->subtitle; ?></div>
                        <div class="hero__title<?php echo ($this->model->page == 'home' && !Utilidades::checkLogin()) ? ' mb-5' : ''; ?>"><?php echo $this->model->title; ?></div>   
                    <?php endif; ?>

                    <?php if($this->model->page == 'home' && !Utilidades::checkLogin()): ?>
                    <a href="/usuario" class="button button--light scrollto">Crear cuenta</a>
                    <?php endif; ?>                                    
                </div>
            </div>            
        </div>        

    </div>

    <?php if($this->model->page !== 'home'): ?>
    <button onclick="window.history.back()" class="button back"><img src="/assets/img/back.svg" width="18" class="mr-2" />Volver atrás</button>
    <?php endif; ?>    

    <?php if(Utilidades::checkLogin() && Utilidades::tipoUsuario($_SESSION['id']) == 'administrador') : ?>                                       
    <div class="admin-bar extras mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">                
                <a href="/admin/crear-producto" class="mr-3">Nuevo producto</a><a href="/admin" class="mr-3">Administrar productos</a><a href="/admin/usuarios" class="mr-3">Administrar usuarios</a><a href="/admin/valoraciones">Administrar valoraciones</a>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>