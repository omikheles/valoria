<section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="decorate text-center mb-5">Productos o servicios <strong>en Valoria</strong></h1>                                        
                    
                    <div class="form-row justify-content-md-center mb-5">
                        <div class="form-group col-md-6">
                            <select class="form-control form-control-lg" name="filtro-categoria">
                                <option value="0">Filtrar por categoría</option>
                                <option value="0">Todas</option>                
                                <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                                <?php echo $categoria->Id; ?>
                                <option <?php echo isset($_GET['id_cat']) && $_GET['id_cat'] == $categoria->Id ? 'selected' :  '' ?> value="<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        
                        <?php foreach(Utilidades::listar_productos() as $producto): ?>                        
                        <div class="col-md-4 mb-5">
                            <div class="card p-3" data-id="<?php echo $producto->Id; ?>">                                
                                <div class="card__image mb-2 d-none d-md-block" style="background-image:url(<?php echo $producto->Imagen; ?>);"></div>
                                <img class="producto__image-mobile d-md-none mb-2" src="<?php echo $producto->Imagen; ?>" />
                                <div class="card__title"><?php echo $producto->Titulo; ?></div>
                                <div class="rating__stars rating__stars--sm mr-2 mb-2"><?php echo Utilidades::estrellasProducto(Utilidades::obtenerPesoValoraciones($producto->Id),true); ?></div>   
                                <div class="card__text"><?php echo Utilidades::extracto($producto->Descripcion,10); ?></div>
                                <div class="text-right"><span class="card__category"><a href="/?id_cat=<?php echo $producto->Id_categoria; ?>"><?php echo Utilidades::obtenerNombreCategoria($producto->Id_categoria); ?></a></span></div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        
                    </div>                    
                                        
                </div>
            </div>            
        </div>        
    </section>    

    <?php if(!Utilidades::checkLogin()): ?>
    <div class="container">
        <div class="row">
            <div class="col">
                <hr class="mt-5"/>
            </div>
        </div>
    </div>    

    <section class="p-5">
            <div class="container">
                <div class="row">
                    <div class="col text-center">                                                                                          
                        <h2 class="decorate text-center mb-5">¿Te gustaría valorar <strong>los productos</strong>?</h2>                            
                        <p class="mb-5">¡Bienvenido! Te invitamos a crear vuestra cuenta personal y totalmente gratuita. Rellena el siguiente formulario para empezar a valorar los productos.</p>          
                        <a href="/usuario" class="button scrollto">Regístrate</a>    
                    </div>
                </div>            
            </div>        
    </section>    

    <?php endif; ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <hr class="mt-5"/>
            </div>
        </div>
    </div>

    <section class="p-5" id="contactar">
        <div class="container">
            <div class="row mb-5">
                <div class="col text-center">
                    <h1 class="decorate text-center mb-5">Contactar con <strong>Valoria</strong></h1>                    
                    
                    <p>Puede contactar con nosotros a través de estos medios:</p>
                                        
                </div>                                
            </div>

            <div class="row">
                <div class="col">
                    <div class="text-center d-none result">
                        <img src="/assets/img/checked.svg" width="100" alt="ok" />
                    </div>
                    <form class="contact-form" action="" method="POST" id="contact">
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">                                    
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">  
                                <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                            </div>                                
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">
                                <button type="submit" class="button">Enviar mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>