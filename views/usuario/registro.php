<?php
// var_dump($this->model);
?>
<section class="p-5">
        <div class="container">            
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">
                    
                    <p class="mb-5"><strong>¡Bienvenido!</strong> Te invitamos a crear vuestra cuenta personal y totalmente gratuita. Rellena el siguiente formulario para empezar a valorar los productos.</p>          
                    
                    <h2 class="mb-5">Datos de usuario</h2>
                    <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>
                    <form class="register-form" action="/usuario/" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $this->model->nombre; ?>" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" value="<?php echo $this->model->apellidos; ?>">
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="<?php echo $this->model->email; ?>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="password" name="contrasena" placeholder="Contraseña" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="email" name="contrasena2" placeholder="Repitir contraseña" required>
                            </div>
                        </div>                        
                        <div class="form-row"> 
                            <div class="form-group col text-right d-md-flex justify-content-end align-items-center">
                                <p class="m-0 mr-md-3 mb-3 mb-md-0 fade-text policy">Estoy de acuerdo con la política de privacidad.</p>
                                <button type="submit" name="registrar" class="button">Registrar cuenta</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>   