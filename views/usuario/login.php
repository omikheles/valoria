<section class="p-5">
        <div class="container">            
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-5">                                        
                    
                    <h2 class="mb-5">Datos de usuario</h2>
                    <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>
                    <form class="register-form" action="/usuario/login" method="POST">                        
                        <div class="form-row">
                            <div class="form-group col-md-12">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="<?php echo $this->model->email; ?>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">                                    
                                <input type="password" class="form-control" id="password" name="contrasena" placeholder="Contraseña" required>
                            </div>                            
                        </div>                        
                        <div class="form-row"> 
                            <div class="form-group col text-right d-md-flex justify-content-end align-items-center">                            
                                <button type="submit" name="login" class="button">Entrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>   