<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-10">
                <h2>Editar producto</h2>
                
                <?php //var_dump($producto); ?>

                <div class="errors text-center mt-5 mb-5"><?php echo $this->model->errors; ?></div>
                    <form class="admin-form-producto" action="/admin/editar-producto?id=<?php echo $id; ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-12">                                    
                                <input type="text" class="form-control" id="titulo" name="titulo_producto" placeholder="Título" value="<?php echo !empty($producto->Titulo) ? $producto->Titulo : null; ?>">                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="admin-form-producto__image-upload p-3">
                                <label class="mr-md-2 d-inline-block">Subir una imagen (máximum 300kb)</label>
                                <input type="file" name="fichero" id="fichero">
                                </div>
                                <small class="form-text text-muted">Separados por coma</small>  
                            </div>
                        </div>
                        <div class="form-row">                            
                            <div class="form-group col-md-4">                                                                    
                                <select class="form-control form-control-lg" name="categoria_producto">
                                    <option value="">Seleccionar categoría</option>                                    
                                    <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                                    <option <?php echo $producto->Id_categoria == $categoria->Id ? 'selected' :  '' ?> value="<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-8">                                    
                                <input type="text" class="form-control" id="ubicacion_producto" name="ubicacion_producto" placeholder="Ubicación de servicio o tienda física." value="<?php echo !empty($producto->Ubicacion) ? $producto->Ubicacion : null; ?>">                                
                            </div>
                        </div>
                        <div class="form-row">                            
                            <div class="form-group col-md-4">                                                           
                                <input type="text" class="form-control" id="email_producto" name="email_producto" placeholder="E-mail de contacto" value="<?php echo !empty($producto->Email) ? $producto->Email : null; ?>">                                         
                            </div>
                            <div class="form-group col-md-4">                                                           
                                <input type="text" class="form-control" id="phone_producto" name="phone_producto" placeholder="Teléfono" value="<?php echo !empty($producto->Phone) ? $producto->Phone : null; ?>">                                         
                            </div>
                            <div class="form-group col-md-4">                                                           
                                <input type="text" class="form-control" id="web_producto" name="web_producto" placeholder="Web" value="<?php echo !empty($producto->Web) ? $producto->Web : null; ?>">                                         
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <textarea class="form-control" id="cuerpo_producto" name="cuerpo_producto" rows="3" placeholder="Cuerpo de producto"><?php echo !empty($producto->Descripcion) ? $producto->Descripcion : null; ?></textarea>
                            </div>
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">                                
                                <button type="submit" name="editar-producto" class="button">Editar</button>
                            </div>
                        </div>                        
                    </form>

            </div>
        </div>
    </div>
</section>