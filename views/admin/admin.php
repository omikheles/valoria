<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12">                                 

            <h2 class="mb-5">Administrar los productos y servicios publicados:</h2>        

            <div class="errors text-center mt-5 mb-5"><?php echo $this->model->errors; ?></div>            

            <div class="form-row">
                <div class="form-group col-md-6">

                    <select class="form-control form-control-lg mb-3" name="filtro-categoria">
                        <option value="">Filtrar por categoría</option>
                        <option value="0">Todas</option>                
                        <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                        <option <?php echo isset($_GET['id_cat']) && $_GET['id_cat'] == $categoria->Id ? 'selected' :  '' ?> value="<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>    

            <?php if(Utilidades::listar_productos()): ?>

            <?php foreach(Utilidades::listar_productos() as $producto): ?>
                    <div class="row product-unit d-md-flex align-items-center pt-4 pb-4">
                        <div class="col-12 col-md-8 mb-3 mb-md-0">
                            <div class="product-unit__title"><a href="/producto?id=<?php echo $producto->Id; ?>"><?php echo $producto->Titulo; ?></a></div>
                            <div class="product-unit__meta"><?php echo Utilidades::obtenerNombreCategoria($producto->Id_categoria) ?></div>
                        </div>
                        <div class="col-12 col-md-4 text-md-right product-unit__tools">
                            <a href="/admin/editar-producto?id=<?php echo $producto->Id; ?>" class="mr-3 pl-3 pr-3">Editar</a><a href="/admin/eliminar-producto?id=<?php echo $producto->Id; ?>" class="pl-3 pr-3">Borrar</a>
                        </div>
                    </div>            
            <?php endforeach; ?>

            <?php else: ?>
            <div class="text-center">
            <p class="mb-5">Productos no encontrados.</p>
            <a href="/admin/crear-producto" class="button scrollto">Crear producto</a> 
            </div>
            <?php endif; ?>

            </div>
        </div>
    </div>
</section>