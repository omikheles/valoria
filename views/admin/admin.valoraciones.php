<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12">                                                          

            <h2 class="mb-5">Administrar las Valoraciones:</h2>                    

            <?php foreach($this->model->listar_opiniones() as $opinion): ?>
                    <div class="row product-unit d-md-flex align-items-center pt-4 pb-4">
                        <div class="col-12 col-md-8 mb-3 mb-md-0">
                            <div class="product-unit__title"><strong><?php echo $opinion->Titulo; ?></strong></div>
                            <div class="rating__stars rating__stars--sm mr-2 mb-2"><?php echo Utilidades::estrellasProducto($opinion->Valoracion,true); ?></div>                        
                            <div class="product-unit__meta mb-2">Publicado por: <a href="/usuario/editar?id=<?php echo $opinion->Id_usuario; ?>"><?php echo Utilidades::nombreUsuario($opinion->Id_usuario); ?></a> | Fecha: <?php echo $opinion->Fecha_valoracion; ?> | <a href="/producto?id=<?php echo $opinion->Id_producto_servicio; ?>">Ver el producto</a> </div>
                            <div><?php echo $opinion->Opinion; ?></div>
                        </div>
                        <div class="col-12 col-md-4 text-md-right product-unit__tools">
                            <a href="/admin/eliminar-opinion?id=<?php echo $opinion->Id; ?>" class="pl-3 pr-3">Borrar</a>
                        </div>
                    </div>            
            <?php endforeach; ?>         

            </div>
        </div>
    </div>
</section>