<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12">                                                          

            <h2 class="mb-5">Administrar los usuarios:</h2>                    

            <?php foreach($this->model->listar_usuarios() as $usuario): ?>
                    <div class="row product-unit d-md-flex align-items-center pt-4 pb-4">
                        <div class="col-12 col-md-8 mb-3 mb-md-0">
                            <div class="product-unit__title"><a href="/usuario/editar?id=<?php echo $usuario->Id; ?>"><?php echo $usuario->Nombre; ?> <?php echo $usuario->Apellidos; ?></a></div>
                            <div class="product-unit__meta"><?php echo $usuario->Email; ?>| Valoraciones (<?php echo $this->model->countValoracionesUsuario($usuario->Id); ?>)</div>
                        </div>
                        <div class="col-12 col-md-4 text-md-right product-unit__tools">
                            <a href="/usuario/editar?id=<?php echo $usuario->Id; ?>" class="mr-3 pl-3 pr-3">Editar</a><a href="/admin/eliminar-usuario?id=<?php echo $usuario->Id; ?>" class="pl-3 pr-3">Borrar</a>
                        </div>
                    </div>            
            <?php endforeach; ?>         

            </div>
        </div>
    </div>
</section>