<?php

class Utilidades
{
    
    /**
     * Terminar la sesión
     */
    public function session_end(){      
        session_destroy();        
    }

    /**
     * Obtener el nombre de la pagina
     */
    public function getPageName(){
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        $routes = !empty($routes[1]) ? $routes[1] : null;
        return strtolower($routes);
    }

    /**
     * Obtener la accion de pagina. El segundo nivel de la URL
     */
    public function getPageAction(){
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        $routes = !empty($routes[2]) ? $routes[2] : null;
        return strtolower($routes);
    }

    /**
     * Comprobamos si el usuario esta autorizado
     */
    public function checkLogin(){
        if (isset($_SESSION['login']) && isset($_SESSION['id'])) {
            return true;
        }
    }

    /**
     * Conseguimos el nombre de tipo de usuario autorizado
     * @param int $id El ID de usuario
     */
    public function tipoUsuario($id,$model = null){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT id_tipo_usuario FROM usuario WHERE Id = ? LIMIT 1";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                  
                $id_tipo = $stm->fetchColumn();
                $stm = "SELECT Nombre FROM tiposusuario WHERE Id = ? LIMIT 1";
                $stm = $pdo->prepare($stm);
                $stm->execute(array($id_tipo));
                return $stm->fetchColumn();
            }            

        } catch (Exception $e) {                        
            die($e->getMessage());
        }
        
        return null;
    }

    /**
     * Recuperamos el nombre de usuario
     * @param int $id El de usuario
     */
    public function nombreUsuario($id){ 
        $pdo = Database::StartUp();

        try 
		{
            
            $stm = "SELECT Nombre FROM usuario WHERE Id = ? LIMIT 1";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                  
                return $stm->fetchColumn();                
            }

        } catch (Exception $e) {
            echo $e->getMessage();     
        }
        
        return false;
    }    

    /**
     * Listar todas las categorias disponibles
     */
    public function listarCategorias()
    {
        $pdo = Database::StartUp();
        try
		{            
			$stm = $pdo->prepare("SELECT * FROM categorias ORDER BY Nombre ASC");
            $stm->execute();            
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
        }
    }

    /**
     * Listar todos los productos     
     */
    public function listar_productos(){
        $pdo = Database::StartUp();
        $id_categoria = !empty($_GET['id_cat']) ? Utilidades::sanitize($_GET['id_cat']) : null;

        try
		{		                                            
            $stm = !empty($id_categoria) ? $pdo->prepare("SELECT * FROM productos_servicios WHERE Id_categoria = ? ORDER BY Id DESC") : $pdo->prepare("SELECT * FROM productos_servicios ORDER BY Id DESC");
            $stm->execute(array($id_categoria));            
            return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
        return false;
    }

    /**
     * Calculamos la Media ponderada o el peso medio de las valoraciones de producto por el ID
     * Pueden encontrar la dinición matemática aquí https://es.wikipedia.org/wiki/Media_ponderada
     * @param int $id El de categoria
     */
    public function obtenerPesoValoraciones($id){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT SUM(x.num * x.Valoracion) / SUM(x.num) FROM (SELECT v.Valoracion, COUNT(v.Valoracion) AS num FROM valoraciones v WHERE v.Id_producto_servicio = ? GROUP BY v.Valoracion) x";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return number_format((float)$stm->fetchColumn(), 2, '.', '');                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
        }
        return false;
    }

    /**
     * Construimos las estrellas en base de peso de las valoraciones
     * @param int $peso El peso
     * @param bool $dark
     */
    public function estrellasProducto($peso,$dark=false){             
        $peso = floor($peso);        
        $stars = '';
        $empty_stars = 5-$peso;

        for ($x = 1; $x <= $peso; $x++) {
            $stars .= '<img src="/assets/img/star.svg" width="30" alt="" />';
        }

        if($peso<5){
            if(!$dark){
                for ($x = 1; $x <= $empty_stars; $x++) {
                    $stars .= '<img src="/assets/img/star-white.svg" width="30" alt="" />';
                }
            } else {
                for ($x = 1; $x <= $empty_stars; $x++) {
                    $stars .= '<img src="/assets/img/star-grey.svg" width="30" alt="" />';
                }
            }
            
        }
        return $stars;
    }
    
    /**
     * Contabilizamos las opiniones de producto por el ID de producto
     * @param $id El id de producto
     */
    public function countOpinionesProducto($id){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT count(*) FROM valoraciones WHERE Id_producto_servicio = ?";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
        }
        return false;
    }   

     /**
     * Obtener el nombre de categoria por el id
     * @param int $id El de categoria
     */
    public function obtenerNombreCategoria($id){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT Nombre FROM categorias WHERE Id = ? LIMIT 1";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
        }
        return false;
    }
    

    /**
     * Limpiamos cualquier valor
     * @param string $field el valor
     */
    public function sanitize($field)
    {        
        $field = strip_tags($field);
        $field = trim($field);
        $field = stripslashes($field);
        return $field = htmlspecialchars($field);                        
    }

    /**
     * Limitar texto
     * @param string $text El texto
     * @param int $limit Cantidad
     */
    public function extracto($text,$limit)
    {           
        $text = strip_tags($text);        

        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }

        return $text;

    }

}
