$(function(){
        
    $('.scrollto').on('click',function(){
        var el = $(this).attr('href');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(el).offset().top
        }, 1500);        
    });

    $('.card').on('click',function(){
        var id = $(this).data('id');
        if(id){
            window.location.href = '/producto?id='+id;
        }
    });

    /**
     * Redirección para select de categoría
     */
    $('select[name=filtro-categoria]').on('change', function() {        
        window.location.href = '?id_cat='+this.value;        
    });

    /**
     * Formulario de contacto
     */
    (function(){

        var form = $('form[id=contact]');

        form.on('submit',function(e){

            e.preventDefault();            

            $.ajax({
                type: 'POST',
                url: "/contactar",
                data: form.serialize(),
                cache: false,
                success: function(data){                        
                },
                statusCode: {
                    200: function(){
                        form.addClass('d-none');
                        $('.result').removeClass('d-none');
                    },
                    404: function(){
                        console.log('something is wrong');
                    }
                }
            });

        });

    })();    


    /**
     * CKeditor
     */
    (function(){
        if(!$('#cuerpo_producto,#cuerpo_opinion').length)return;

        ClassicEditor
            .create(document.querySelector( '#cuerpo_producto,#cuerpo_opinion' ),{
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
            })
            .catch( error => {
                console.error( error );
            });

    })();

    /**
     * Estrellas de opinion
     */
    (function(){
        var star = $('.rating__star');
        var radioCheckedIndex = $(":radio[name='stars_opinion']").index($(":radio[name='stars_opinion']:checked"))+1;
        console.log(radioCheckedIndex);

        for (let s = 0; s < radioCheckedIndex;) {                    
            $('.rating__star').eq(s).addClass('active');
            $('.rating__star').eq(s).addClass('active-fixed');                    
            s++
        }
        
        star.on('mouseover',function(){                    
            if($('.rating__star').hasClass('active-fixed'))return;            
            for (let index = 0; index < $(this).index(); index++) {                
                $('.rating__star:eq('+index+')').addClass('active');
            }
        });        

        star.on('click',function(){
            var index = $(this).index();
                
            console.log(index);
                        
            $('.rating__star').removeClass('active');
            $('.rating__star').removeClass('active-fixed');
                
                for (let s = 0; s < index;) {                    
                    $('.rating__star').eq(s).addClass('active');
                    $('.rating__star').eq(s).addClass('active-fixed');                    
                    s++
                }

                index = index - 1;    
                               
                $('input[name=stars_opinion]').removeAttr('checked');
                $('input[name=stars_opinion]:eq('+index+')').attr('checked','checked');                
                index = 1;
        });

        star.on('mouseout',function(){            
            if($('.rating__star').hasClass('active-fixed'))return;            
            
            $('.rating__star').removeClass('active');                            
        });

    })();
    

});