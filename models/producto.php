<?php

class Producto {
    
    private $pdo;

    public $utilidades;
    public $errors;    

    public $title;
    public $subtitle;    
    public $page;    
    public $action;
        
    public $id_usuario;    
    public $id_producto;   

    public $titulo_opinion;
    public $cuerpo_opinion;
    public $stars_opinion;

    /**
     * Constructor de modelo Producto
     */
    public function __construct(){
                
        $this->utilidades = new Utilidades();
        
        $this->page = $this->utilidades->getPageName();
        $this->action = $this->utilidades->getPageAction();                
        
        $this->id_usuario = isset($_SESSION['id']) ? $_SESSION['id'] : null;
        $this->title = $this->utilidades->checkLogin() ? $this->utilidades->nombreUsuario($this->id_usuario) : '';
        $this->subtitle = 'Bienvenido';
                    
        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Obtenemos el objeto de producto completo por el ID
     * @param int $id El id de producto
     */
    public function obtenerProducto($id){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM productos_servicios WHERE Id = ?");
            $stm->execute(array($id));       
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            die($e->getMessage());
        }
        
        return false;
    }

    /**
     * Listamos las opiniones de producto
     * @param int $id El ID de producto
     */
    public function listar_opiniones($id){                
        try
		{		                                            
            $stm = $this->pdo->prepare("SELECT * FROM valoraciones WHERE Id_producto_servicio = ? ORDER BY Id DESC");
            $stm->execute(array($id));            
            return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
        return false;
    }

    /**
     * Obtener opinion por el id de usuario y producto
     * @param int $id_usuario El id de usuario
     * @param int $id_producto El id de producto
     */
    public function obtenerOpinion($id_usuario,$id_producto){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM valoraciones WHERE Id_usuario = ? AND Id_producto_servicio = ?");
			$stm->execute(array($id_usuario,$id_producto));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();        
        }
        return false;
    }

    /**
     * Valorar producto
     * @param array $data Datos recuperados desde el modelo
     */
    public function valorar_producto($data){
        try {
            
            // Campos obligatorios                        
            if(empty($data->stars_opinion) || empty($data->titulo_opinion) || empty($data->cuerpo_opinion)){
                throw new Exception("Los campos Estrellas, Titulo y el texto de opinión son obligatorios.");
            }    

            // Comprobamos si valoración ya existe para el mismo usuario y producto
            if(!$this->opinionUnica($data->id_usuario,$data->id_producto)){
                throw new Exception("Parece que ya tiene valorado este producto o servicio.");   
            }
            
            $stm = "INSERT INTO valoraciones (Titulo,Opinion,Valoracion,Id_usuario,Id_producto_servicio,Fecha_valoracion) 
            VALUES (?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($stm)
            ->execute(
                array(
                    $data->titulo_opinion,
                    $data->cuerpo_opinion,                                                        
                    $data->stars_opinion,
                    $data->id_usuario,
                    $data->id_producto,
                    date('Y-m-d H:i:s')                 
                )
            );

            return true;
            
        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}
        return false;
    }

    public function modificarOpinion($data){
        
        try 
		{

            // Campos obligatorios                        
            if(empty($data->stars_opinion) || empty($data->titulo_opinion) || empty($data->cuerpo_opinion)){
                throw new Exception("Los campos Estrellas, Titulo y el texto de opinión son obligatorios.");
            }
            
            $stm = "UPDATE valoraciones SET 
				        Titulo      = ?, 
						Opinion   = ?,
                        Valoracion       = ?,
						Fecha_valoracion    = ?
                WHERE Id_usuario = ? AND Id_producto_servicio = ?";
                
                $this->pdo->prepare($stm)
                ->execute(
                   array(
                        $data->titulo_opinion,
                        $data->cuerpo_opinion,
                        $data->stars_opinion,                       
                        date('Y-m-d H:i:s'),
                        $data->id_usuario,
                        $data->id_producto
                   )
               );
               
               return true;

        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
        }
        
        return false;

    }

    /**
     * Comprobamos si valoración ya existe para el mismo usuario y producto
     * @param int $id_usuario El id de usuario
     * @param int $id_producto El id de producto
     */
    private function opinionUnica($id_usuario,$id_producto){
        try
		{                                            

            $stm = $this->pdo->prepare("SELECT * FROM valoraciones WHERE Id_usuario = ? AND Id_producto_servicio = ? LIMIT 1");
            $stm->execute(array($id_usuario,$id_producto));

            if($stm->rowCount() > 0){
                return false;
            }            

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
        }
        
        return true;
    }
    
}