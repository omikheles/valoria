<?php

class Home {
    
    private $pdo;
    public $title;
    public $subtitle;
    public $pagecontent;
    public $page;
    public $utilidades;    
    public $id_usuario;

    /**
     * Constructor de modelo Home
     */
    public function __construct(){
        $this->utilidades = new Utilidades();
        $this->page = 'home';
        $this->id_usuario = isset($_SESSION['id']) ? $_SESSION['id'] : null;
        $this->title = $this->utilidades->checkLogin() ? $this->utilidades->nombreUsuario($this->id_usuario) : 'Tu opinión importa';        
        $this->subtitle = $this->utilidades->checkLogin() ? 'Bienvenido' : '¿Te gustaría valorar los productos?';

        try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

}