<?php

class Usuario {
    
    private $pdo;    
    public $utilidades;
    public $errors;    

    public $title;
    public $subtitle;    
    public $page;
    
    public $id;
    public $nombre;
    public $apellidos;
    public $email;
    public $contrasena;
    public $contrasena2;
    

    /**
     * Constructor de modelo Usuario
     */
    public function __construct(){       
        
        $this->utilidades = new Utilidades();
                
        // Asignamos algunos datos estáticos para varias vistas
        $page = $this->utilidades->getPageName();
        $action = $this->utilidades->getPageAction();
            
        if($page == 'usuario' && $action == null || $action == 'registrar'){
            $this->page = $page;
            $this->title = 'Crear cuenta';
            $this->subtitle = 'Tu opinión importa';
        } elseif($page == 'usuario' && $action == 'editar' || $action == 'actualizar'){
            $this->title = 'Bienvenido';
            $this->subtitle = 'Editar perfil';
        } elseif($page == 'usuario' && $action == 'login'){
            $this->title = 'Iniciar sesión';
            $this->subtitle = 'Bienvenido';
        }
            
        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }   

    /**
     * Registramos el usuario con los datos de formulario
     */
    public function registrar($data){        

        $this->pdo->beginTransaction();

        try 
		{
                    
        // Campos obligatorios
        if(empty($data->nombre) || empty($data->email) || empty($data->contrasena) || empty($data->contrasena2)){
            throw new Exception("Los campos Nombre, E-mail y contraseña son obligatorios.");
        }
        
        // Comprobamos si el usuario existe            
        if(self::usuarioExiste($data->email)){
            throw new Exception("El usuario ya existe.");
        }
        
        // Comprobamos si las contraseñas publicadas coinciden
        if($data->contrasena !== $data->contrasena2){
            throw new Exception("Las contraseñas no coinciden.");             
        }

        $stm = "INSERT INTO usuario (Nombre,Apellidos,Email,Password,id_tipo_usuario) 
                VALUES (?, ?, ?, ?, ?)";
		$this->pdo->prepare($stm)
		    ->execute(
			    array(
                    $data->nombre,
                    $data->apellidos,
                    $data->email,
                    $data->contrasena,
                    2
                )
            );
            
        $this->id = $this->pdo->lastInsertId();
        $this->pdo->commit();
        
        return true;

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        }
         catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Obtenemos el objeto de usuario desde la base
     * @param int $id El id recuperado desde la sesion
     */
    public function obtener($id){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM usuario WHERE id = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();        
        }
        
    }

    /**
     * Actualizar datos de usuario
     * @param object $data El objeto de usuario
     */
    public function actualizar($data){        

        $this->pdo->beginTransaction();

        try 
		{
            
        // Campos obligatorios
        if(empty($data->nombre) || empty($data->email) || empty($data->contrasena) || empty($data->contrasena2)){
            throw new Exception("Los campos Nombre, E-mail y contraseña son obligatorios.");
        }        

        // Comprobamos si las contraseñas publicadas coinciden
        if($data->contrasena !== $data->contrasena2){
            throw new Exception("Las contraseñas no coinciden.");             
        }

        if(self::emailReservado($data->email,$data->id)){
            throw new Exception("El e-mail ya esta reservado por el otro usuario.");
        }

        $stm = "UPDATE usuario SET 
				        Nombre      = ?, 
						Apellidos   = ?,
                        Email       = ?,
						Password    = ?
				WHERE Id = ?";

			$this->pdo->prepare($stm)
			     ->execute(
				    array(
                        $data->nombre,
                        $data->apellidos,
                        $data->email,                                                
                        $data->contrasena,
                        $data->id
					)
                );            
                        
            $this->pdo->commit();
        
            return true;

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        }
         catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }
    
    /**
     * Login principal
     * @param string $email El email de usuario como nombre de usuario
     * @param string $password La contraseña
     */
    public function login($email,$password){
        try 
		{
            
            $stm = "SELECT Id,id_tipo_usuario FROM usuario WHERE Email = ? AND Password = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($email,$password));

            if($stm->rowCount() > 0){                        
                return $stm->fetch();
            }

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Comprobamos si el usuario existe en la base en el momento de registrarlo
     * @param string $email El email introducido como nombre de usuario
     */
    private function usuarioExiste($email){

        try 
		{
            
            $stm = "SELECT Email FROM usuario WHERE Email = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($email));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

    }

    /**
     * Comprobamos si el email esta reservado por el otro usuario. Utilizado en el momento de editar el perfil.
     */
    private function emailReservado($email,$id){
        try 
		{
            
            $stm = "SELECT Email FROM usuario WHERE Email = ? AND Id != ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($email,$id));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

}