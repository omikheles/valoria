<?php

class Admin {
    
    private $pdo;

    public $utilidades;
    public $errors;    

    public $title;
    public $subtitle;    
    public $page;    
    public $action;
        
    public $id_usuario;

    public $id_producto;   
    public $titulo_producto;
    public $cuerpo_producto;
    public $categoria_producto;
    public $image_url;

    public $ubicacion_producto,$web_producto,$email_producto,$phone_producto;

    /**
     * Constructor de modelo Admin
     */
    public function __construct(){
                
        $this->utilidades = new Utilidades();
        
        $this->page = $this->utilidades->getPageName();
        $this->action = $this->utilidades->getPageAction();                
        
        $this->id_usuario = isset($_SESSION['id']) ? $_SESSION['id'] : null;
        $this->title = $this->utilidades->checkLogin() ? $this->utilidades->nombreUsuario($this->id_usuario) : '';
        $this->subtitle = 'Bienvenido';
                    
        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Crear el producto nuevo dentro de la base
     * @param array $data Datos de producto
     */
    public function crear_producto($data){
        
        try {            
        
            // Campos obligatorios
            if(empty($data->titulo_producto) || empty($data->cuerpo_producto) || empty($data->categoria_producto)){
                throw new Exception("Los campos Título, Categoría y Cuerpo son obligatorios.");
            }    

            // Comprobamos si el producto ya existe por el titulo
            if(self::productoExiste($data->titulo_producto)){
                throw new Exception("Parece que el mismo producto ya existe.");
            }

            // Subimos el archivo
            Self::fileUpload();

            // Insertamos el producto
            $stm = "INSERT INTO productos_servicios (Titulo,Descripcion,Imagen,Ubicacion,Web,Email,Phone,Id_categoria) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($stm)
            ->execute(
                array(
                    $data->titulo_producto,
                    $data->cuerpo_producto,
                    $data->image_url,                                        
                    $data->ubicacion_producto,
                    $data->web_producto,
                    $data->email_producto,
                    $data->phone_producto,
                    $data->categoria_producto
                )
            );            

            return true;

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

        return false;
    }

    /**
     * Actualizar producto
     * @param array $data Datos de producto
     */
    public function actualizar_producto($data){
        
        try {            
        
            // Campos obligatorios
            if(empty($data->titulo_producto) || empty($data->cuerpo_producto) || empty($data->categoria_producto)){
                throw new Exception("Los campos Título, Categoría y Cuerpo son obligatorios.");
            }    

            // Comprobamos si el producto ya existe por el titulo
            if(self::productoExiste($data->titulo_producto)){
                throw new Exception("Parece que el mismo producto ya existe.");
            }

            // Subimos el archivo
            Self::fileUpload();

            // Actualizamos el producto            
            $stm = "UPDATE productos_servicios SET Titulo = ?,Descripcion = ?,Imagen = ?,Ubicacion = ?,Web = ?,Email = ?,Phone = ?,Id_categoria = ? WHERE Id = ?";

            $this->pdo->prepare($stm)
            ->execute(
                array(
                    $data->titulo_producto,
                    $data->cuerpo_producto,
                    $data->image_url,                                        
                    $data->ubicacion_producto,
                    $data->web_producto,
                    $data->email_producto,
                    $data->phone_producto,
                    $data->categoria_producto,
                    $data->id_producto
                )
            );            

            return true;

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

        return false;
    }

    /**
     * Eliminar Producto
     * @param int $id El id de producto
     */
    public function eliminar_producto($id_producto){

        $this->pdo->beginTransaction();

        try {                                   
            
            // $this->eliminarValoracion($id_producto);

            $stm = $this->pdo->prepare("SELECT * FROM valoraciones WHERE Id_producto_servicio = $id_producto");
            $stm->execute();         

            if($stm->rowCount() > 0){                
                throw new Exception("No se puede borrar el producto porque tiene las valoraciones de los usuarios.");
            }            
            
            $stm = "DELETE FROM productos_servicios WHERE Id = ?";

            $this->pdo->prepare($stm)
			     ->execute(array($id_producto));
            
            $this->pdo->commit();                    

            return true;            

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
		}
    }

    /**
     * Eliminar valoracion por el ID de producto
     * @param int $id_producto El id de producto
     */
    public function eliminarValoracion($id_producto){
        $stm = "DELETE FROM valoraciones WHERE Id_producto_servicio = ?";
                
        $this->pdo->prepare($stm)
            ->execute(array($id_producto)
        );
    }    

    /**
     * Comprobamos si el producto existe
     */
    private function productoExiste($titulo){
        try
		{                        
            
            $stm_array = $this->action == 'editar-producto' ? array($titulo,$this->id_producto) : array($titulo);         

            $stm = $this->action == 'editar-producto' ? "SELECT Titulo FROM productos_servicios WHERE Titulo = ? AND Id != ? LIMIT 1" : "SELECT Titulo FROM productos_servicios WHERE Titulo = ? LIMIT 1";

            $stm = $this->pdo->prepare($stm);
            $stm->execute($stm_array);

            if($stm->rowCount() > 0){
                return true;
            }            

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
        }
        
        return false;
    }

    /**
     * Obtenemos el objeto de producto completo por el ID
     * @param int $id El id de producto
     */
    public function obtenerProducto($id){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM productos_servicios WHERE Id = ?");
            $stm->execute(array($id));       
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            die($e->getMessage());
        }
        
        return false;
    }

    /**
     * Subir el archivo
     */
    public function fileUpload(){        
        
        if(empty($_FILES["fichero"]["tmp_name"])){
            return false;
        }

        $target_dir = "uploads/";
        $target_file = $this->image_url = $target_dir . basename($_FILES["fichero"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));        
        
        // Comprobamos si la imagen es real.
        if(!getimagesize($_FILES["fichero"]["tmp_name"])){
            throw new Exception("Vuestra imagen parece falsa. Solo permitidos JPG, PNG. Máximo 300kb.");
        }

        // Comprobamos el tamaño máximo de la imagen.
        if ($_FILES["fichero"]["size"] > 300000) {
            throw new Exception("El fichero es demasiado grande. Máximo permitido 300kb.");            
        }

        // Únicamente permitimos los JPGs y PNGs
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            throw new Exception("El formato no es correcto. Solo permitimos los JPG, PNG.");
        }

        if (!move_uploaded_file($_FILES["fichero"]["tmp_name"], $target_file)) {
            throw new Exception("Ha ocurrido algún problema con vuestra imagen. Por favor, volver a intentar.");
        }
        
        return true;

    }

    /**
     * Listar todos los usuarios
     * @param int $id El ID de usuario
     */
    public function listar_usuarios(){

        try
		{		                                            
            $stm = $this->pdo->prepare("SELECT * FROM usuario ORDER BY Id DESC");
            $stm->execute();            
            return $stm->fetchAll(PDO::FETCH_OBJ);            
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
        return false;
    }

    /**
     * Contamos la cantidad de las valoraciones por el ID de usuario
     * @param int $id_usuario El ID de usuario
     */
    public function countValoracionesUsuario($id_usuario){
        try {
            $stm = $this->pdo->prepare("SELECT count(*) FROM valoraciones WHERE Id_usuario = ?");
            $stm->execute(array($id_usuario));
            return $stm->fetchColumn();
        } catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    /**
     * Eliminar Producto
     * @param int $id El id de producto
     */
    public function eliminar_usuario($id_usuario){

        $this->pdo->beginTransaction();

        try {                                   
            
            $this->eliminarValoracionUsuario($id_usuario);
            
            $stm = "DELETE FROM usuario WHERE Id = ?";

            $this->pdo->prepare($stm)
			     ->execute(array($id_usuario));
            
            $this->pdo->commit();                    

            return true;            

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
		}
    }

    /**
     * Eliminar valoracion por el ID de usuario
     * @param int $id_producto El id de usuario
     */
    public function eliminarValoracionUsuario($id_usuario){
        try {
            
            $stm = "DELETE FROM valoraciones WHERE Id_usuario = ?";
                    
            $this->pdo->prepare($stm)
                ->execute(array($id_usuario)
            );
            
            return true;

        } catch (Exception $e)
        {            
            die($e->getMessage());
        }
        return false;
    }

    /**
     * Eliminar valoracion por el ID de opinion
     * @param int $id_producto El id de opinion
     */
    public function eliminarOpinionPorId($id_opinion){

        try {  

            $stm = "DELETE FROM valoraciones WHERE Id = ?";
                
            $this->pdo->prepare($stm)
                ->execute(array($id_opinion)
            );

            return true;

        } catch (Exception $e)
        {            
            die($e->getMessage());
        }
        return false;
    }

    /**
     * Listamos todas las opiniones
     */
    public function listar_opiniones(){                
        try
		{		                                            
            $stm = $this->pdo->prepare("SELECT * FROM valoraciones ORDER BY Id DESC");
            $stm->execute();            
            return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
        return false;
    }

}